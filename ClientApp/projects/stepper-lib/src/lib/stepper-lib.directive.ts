import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[libStepperLib]'
})
export class StepperLibDirective {

  constructor(public templateRef: TemplateRef<unknown>) { }

}
