import { Component, OnInit, Input, TemplateRef, ContentChild } from '@angular/core';

@Component({
  selector: 'poc-stepper-lib',
  templateUrl: 'stepper-lib.component.html',
  styleUrls: ['stepper-lib.component.scss']
})
export class StepperLibComponent implements OnInit {
  @Input('childTemplate') libStepperLib!: TemplateRef<any>;

  constructor() {}

  ngOnInit() {
    
    console.log(this.libStepperLib.elementRef);
  }
}
