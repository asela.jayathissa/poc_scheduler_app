import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperLibComponent } from './stepper-lib.component';

describe('StepperLibComponent', () => {
  let component: StepperLibComponent;
  let fixture: ComponentFixture<StepperLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepperLibComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StepperLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
