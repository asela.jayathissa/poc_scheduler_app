import { TestBed } from '@angular/core/testing';

import { StepperLibService } from './stepper-lib.service';

describe('StepperLibService', () => {
  let service: StepperLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StepperLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
