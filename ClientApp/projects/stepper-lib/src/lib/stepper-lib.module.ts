import { NgModule } from '@angular/core';
import { StepperLibRoutingModule } from './stepper-lib-routing.module';
import { StepperLibComponent } from './stepper-lib.component';
import {MatStepperModule} from '@angular/material/stepper';
import {MatButtonModule} from '@angular/material/button';
import { StepperLibDirective } from './stepper-lib.directive';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AddressBookLibModule } from 'address-book-lib';


@NgModule({
  declarations: [
    StepperLibComponent,
    StepperLibDirective
  ],
  imports: [
    CommonModule,
    RouterModule,
    StepperLibRoutingModule,
    MatStepperModule,
    MatButtonModule,
    AddressBookLibModule
  ],
  exports: [
    StepperLibComponent,
  ]
})
export class StepperLibModule { }
