/*
 * Public API Surface of address-book-lib
 */

export * from './lib/address-book-lib.service';
export * from './lib/address-book-lib.component';
export * from './lib/address-book-lib.module';
export * from './lib/components/address-book-list/address-book-list.component';
