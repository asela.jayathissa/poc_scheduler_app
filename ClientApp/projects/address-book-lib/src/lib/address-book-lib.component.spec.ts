import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressBookLibComponent } from './address-book-lib.component';

describe('AddressBookLibComponent', () => {
  let component: AddressBookLibComponent;
  let fixture: ComponentFixture<AddressBookLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddressBookLibComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddressBookLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
