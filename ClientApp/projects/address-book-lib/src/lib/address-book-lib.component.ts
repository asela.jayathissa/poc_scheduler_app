import { Component } from '@angular/core';

@Component({
  selector: 'lib-address-book-lib',
  template: `
    <lib-address-book-list></lib-address-book-list>
  `,
  styles: [
  ]
})
export class AddressBookLibComponent {

}
