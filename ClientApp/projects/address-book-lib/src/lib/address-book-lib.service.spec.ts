import { TestBed } from '@angular/core/testing';

import { AddressBookLibService } from './address-book-lib.service';

describe('AddressBookLibService', () => {
  let service: AddressBookLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddressBookLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
