import { NgModule } from '@angular/core';
import { AddressBookLibComponent } from './address-book-lib.component';
import { AddressBookListComponent } from './components/address-book-list/address-book-list.component';



@NgModule({
  declarations: [
    AddressBookLibComponent,
    AddressBookListComponent
  ],
  imports: [
  ],
  exports: [
    AddressBookLibComponent,
    AddressBookListComponent
  ]
})
export class AddressBookLibModule { }
