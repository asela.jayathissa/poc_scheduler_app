import { Component, ContentChild } from '@angular/core';
import { StepperLibDirective } from 'projects/stepper-lib/src/public-api';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'common-stepper';
  @ContentChild(StepperLibDirective) content!: StepperLibDirective;

  steps = [
    {
      id: 1,
      name: 'step 1',
      buttons: ["next"]
    },
    {
      id: 2,
      name: 'step 2',
      buttons: ['back', "next"]
    },
    {
      id: 3,
      name: 'step 3',
      buttons: ['back', "next"]
    }
    ,
    {
      id: 4,
      name: 'step 4',
      buttons: ['finish']
    }
  ]
}
